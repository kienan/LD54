extends "res://src/Plant.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Grows fast, but short and it's tough roots drag rocks up when removed

func _init():
	self.light_range = [20, 80]
	self.base_score = 0
	self.harvest_rockiness_mod = 15
	self.season_nutrient_mod = -15
	self.growth_thresholds = [50, 75, 100, 125, 150]
	self.heights = [0, 1, 1, 1, 2, 2]
	self.plantable = false
	self.icon = preload("res://assets/weed icon.png")
	self.sprites[3] = preload("res://assets/weed plant.png")
	self.sprites[4] = preload("res://assets/weed plant.png")
	self.sprites[5] = preload("res://assets/weed plant.png")
	self.description = "It noms nutrients and its roots pull up lots of rocks"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func use_nutrients_and_run_plant_effects(soil, old_season, new_season, grid):
	# For each soil neighbour, use it's nutrients
	.use_nutrients_and_run_plant_effects(soil, old_season, new_season, grid)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
