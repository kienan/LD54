extends "res://src/Plant.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# grows tall and saps the nutrients from adjoining soil
func _init():
	self.light_range = [5, 95]
	self.base_score = 0
	self.harvest_rockiness_mod = 25
	self.season_nutrient_mod = -10
	self.growth_thresholds = [75, 100, 140, 175, 20000]
	self.heights = [0, 1, 2, 3, 3, 3]
	self.survives_winter = true
	self.plantable = false
	self.icon = preload("res://assets/strong weed icon.png")
	self.sprites[3] = preload("res://assets/strong weed plant.png")
	self.sprites[4] = preload("res://assets/strong weed plant.png")
	self.sprites[5] = preload("res://assets/strong weed plant.png")
	self.description = "Ouch, prickly!"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
