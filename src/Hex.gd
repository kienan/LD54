extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var pf_location = Vector2(-1, -1)
var stored = null
var tooltip_timer = -1.0
var last_light_received = 0
var last_light_timer = 0.0
var last_light_timer_max = 2.0
var arrow_sprite_life_max = 2.0
var arrow_sprite_life = 0.0
var arrow_sprite = preload("res://assets/small arrow.png")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	if tooltip_timer != -1.0:
		if tooltip_timer >= 0.0:
			tooltip_timer -= delta
		if tooltip_timer <= 0.0 and not get_node("Tooltip").is_visible():
			get_node("Tooltip").set_visible(true)
	if self.arrow_sprite_life > 0.0:
		self.arrow_sprite_life -= delta
		if self.arrow_sprite_life <= 0.0:
			get_node("Arrow").queue_free()
	if self.last_light_timer >= 0.0:
		self.last_light_timer -= delta
		if self.last_light_timer <= 0.0:
			get_node("Light").set_visible(false)
			if get_node("Sprite") != null:
				get_node("Sprite").set_modulate(self.get_default_modulate())

func start_tooltip_timer():
	self.tooltip_timer = 0.5

func hide_tooltip():
	get_node("Tooltip").set_visible(false)
	self.tooltip_timer = -1.0

func set_location(x: Vector2):
	self.pf_location = x
	self.update_tooltip("")

func update_tooltip(tooltip):
	if tooltip:
		tooltip += "\n"
	tooltip += "Location: " + str(pf_location.x) + "," + str(pf_location.y)
	get_node("Tooltip").set_text(tooltip)

func query_details():
	return "Location: " + str(pf_location.x) + "," + str(pf_location.y)

func can_do_action(action):
	if action == "query":
		return [true, true, "", 0]
	else:
		return [false, false, "Action not applicable: " + action, 0]

func do_action(action, args):
	pass

func get_height():
	if self.stored == null:
		return 0;
	return self.stored.height

func display_arrow(direction: Vector2, height, width, size):
	var s = Sprite.new()
	s.set_texture(self.arrow_sprite)
	s.set_name("Arrow")
	self.arrow_sprite_life = self.arrow_sprite_life_max
	var pos = Vector2(
		(direction.x * height * 0.75) + (direction.y * width * 0.6),
		(direction.y * size * 1.25)
	)
	s.set_position(pos)
	var angle = PI * float(-direction.x) * 0.5
	angle += PI * float(direction.y) * 0.33
	if direction.y != 0 and direction.x == 0:
		angle += PI
	s.rotate(angle)
	add_child(s)

func apply_light(value):
	# print("[%s] Received light: %s" % [self, value])
	self.last_light_received = value
	get_node("Light").set_text(str(self.last_light_received))
	get_node("Light").set_visible(true)
	self.last_light_timer = self.last_light_timer_max
	if get_node("Sprite") != null and value <= 0:
		get_node("Sprite").set_modulate(Color(0, 0, 0, 0.50))

func get_neighbours(grid, include_storage = false):
	var r = []
	var neighbours = grid.get_neighbours(self.pf_location.x, self.pf_location.y)
	if include_storage:
		return neighbours

func get_default_modulate():
	return Color(1.0, 1.0, 1.0, 1.0)
